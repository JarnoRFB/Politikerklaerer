"""Convert election promises from https://www.sueddeutsche.de/politik/bundesregierung-aufgaben-koalitionsvertrag-aktuell-1.3959524
into structured JSON.

Instructions

1. Go to https://www.sueddeutsche.de/politik/bundesregierung-aufgaben-koalitionsvertrag-aktuell-1.3959524
2. Click "mehr laden" until you have reached the limit.
3. Hit Ctrl + s to save the page.
4. Get the index.html file and put it in the same directory as this scirpt.
5. Install the requirements.
6. Run $ python sz_koalitionstracker_parser.py
"""
import json
import bs4
from bs4 import BeautifulSoup
import dateparser


def main():
    with open("index.html") as fp:
        text = fp.read() 
    soup = BeautifulSoup(text.replace("\n", ""), "html.parser")

    promises = []
    for el in soup.find_all("section", class_="szer-wrapper-container")[0].contents:
        if isinstance(el, bs4.element.Tag) and el.contents[0]["class"][0] == "szer_label_wrapper":
                promises.append(extract_promise(el))

    if len(promises) == 138:
        print("All promises successfully parsed.")
    else:
        print("The number of promises deviates from the expected number 138.")

    json.dump(promises, open("promises.json", "w"))

    
def extract_promise(html_promise):
    current_status = None
    external_links = None
    for p in html_promise.find_all("p"):
        if p.b is not None:
            if "So steht’s im Koalitionsvertrag:" in p.b.text:
                passage_from_treaty = p.contents[1].strip().lstrip("„").rstrip("”")
                link_to_treaty = p.a["href"]
            elif "So ist der Stand:" in p.b.text:
                current_status = ''.join(text_or_str(x) for x in p.contents[1:])
                external_links = [anchor["href"] for anchor in p.find_all("a")]

            elif "Geplant bis: " in p.b.text:
                date_str = p.contents[1].strip().strip(",").replace(u'\xa0', " ")
                deadline = dateparser.parse(date_str, locales=["de"]).isoformat()
    
    promise = dict(
        status=html_promise.find("span", class_="szer_label").text,
        topic=html_promise.find("div", class_="szer_dachzeile").text,
        name=html_promise.find("h2").text,
        passage_from_treaty=passage_from_treaty,
        link_to_treaty=link_to_treaty,
        current_status=current_status,
        external_links=external_links,
        deadline=deadline,
    )
    return promise


def text_or_str(x):
    try:
        return x.text
    except AttributeError:
        return str(x)


if __name__ == "__main__":
    main()

